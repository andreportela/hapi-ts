/// <reference path='../typings/index.d.ts' />

import * as Hapi from 'hapi';
import * as Chai from 'chai';
import Server from '../src/server';

const assert: Chai.Assert = require('chai').assert;
const server: Hapi.Server = Server(3000);

describe('Health Check', () => {

  it('Responds with status code 200 and "Hello World" text', (done) => {

    let options = {
      method: 'GET',
      url: '/'
    };

    server.inject(options, (response) => {

      assert.equal(response.statusCode, 200);
      assert.equal(response.result, 'Hello World');

      done();

    });

  });

});
