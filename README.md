Tools
========

- Gulp
- Typings

Commands
========

- npm install
- typings install
- gulp test
- gulp nodemon
