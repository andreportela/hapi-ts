'use strict'

const path = require('path');
const gulp = require('gulp');
const debug = require('gulp-debug');
const rimraf = require('gulp-rimraf');
const tsc = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const tslint = require('gulp-tslint');
const nodemon = require('gulp-nodemon');
const mocha = require('gulp-mocha');
const istanbul = require('gulp-istanbul');
const remapIstanbul = require('remap-istanbul/lib/gulpRemapIstanbul');

const tsProject = tsc.createProject('tsconfig.json');
const sourceFiles = 'src/**/*.ts';
const testFiles = 'test/**/*.ts';
const outDir = require('./tsconfig.json').compilerOptions.outDir;
const entryPoint = outDir + '/src/index.js';

// cleans the output directory
gulp.task('clean', () => {
  return gulp
    .src(outDir, { read: false })
    .pipe(rimraf());
});

// validates typescripts files
gulp.task('tslint', () => {
  return gulp
    .src(sourceFiles)
    .pipe(tslint())
    .pipe(tslint.report('prose', {
      emitError: false
    }));
});

// compiles typescripts files
gulp.task('compile', ['clean', 'tslint'], () => {
  return gulp
    .src([sourceFiles, testFiles])
    .pipe(sourcemaps.init())
    .pipe(tsc(tsProject))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(outDir));
});

// tests
gulp.task('test', ['compile'], (cb) => {
  gulp
    .src(path.join(outDir, '/src/**/*.js'))
    .pipe(istanbul())
    .pipe(istanbul.hookRequire())
    .once('error', () => {
      process.exit(1);
    })
    .once('end', () => {
      process.exit();
    })
    .on('finish', () => {
      gulp
        .src(path.join(outDir, '/test/**/*.js'), { read: false })
        .pipe(mocha({ reporter: 'list' }))
        .pipe(istanbul.writeReports({ reporters: ['json', 'text-summary'] }))
        // .pipe(istanbul.enforceThresholds({ thresholds: { global: 90 } }))
        .on('finish', () => {
          gulp
            .src('coverage/coverage-final.json')
            .pipe(remapIstanbul({
              reports: {
                'json': 'coverage/coverage-remap.json',
                'html': 'coverage/html'
              }
            }));
        });
    });
});

// whatches the typescripts files and compiles if has been changed
gulp.task('nodemon', ['compile'], () => {
  return nodemon({
    verbose: true,
    script: entryPoint,
    watch: sourceFiles,
    ext: 'ts js json',
    env: { 'NODE_ENV': 'development' },
    tasks: ['compile']
  });
});

// builds
gulp.task('build', ['compile'], () => {
  console.log('Building the project ...');
});
