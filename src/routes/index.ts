/// <reference path='../../typings/index.d.ts' />

'use strict';

import * as Hapi from 'hapi';
import homeRoutes from './home';

const routes: Hapi.IRouteConfiguration[] = [].concat(homeRoutes);

export default routes;
