/// <reference path='../../typings/index.d.ts' />

'use strict';

import * as Hapi from 'hapi';
import * as actions from '../actions/home';

const home: Hapi.IRouteConfiguration = {
  method: 'GET',
  path: '/',
  config: actions.indexAction
};

const routes: Hapi.IRouteConfiguration[] = [home];

export default routes;
