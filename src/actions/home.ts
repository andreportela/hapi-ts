/// <reference path='../../typings/index.d.ts' />

'use strict';

import * as Hapi from 'hapi';

export const indexAction: Hapi.IRouteAdditionalConfigurationOptions = {
  handler: (request: Hapi.Request, reply: Hapi.IReply) => {
    reply('Hello World');
  }
};

export const dummyAction: Hapi.IRouteAdditionalConfigurationOptions = {
  handler: (request: Hapi.Request, reply: Hapi.IReply) => {
    reply('Teste');
  }
};
