/// <reference path='../typings/index.d.ts' />

import Server from './server';

const server = Server(3000);

const options = {
  ops: { interval: 1000 },
  reporters: {
    console: [
      {
        module: 'good-squeeze',
        name: 'Squeeze',
        args: [{ log: '*', response: '*' }]
      },
      { module: 'good-console' }, 'stdout'
    ]
  }
};

server.register(
  {
    register: require('good'),
    options,
  },
  (err) => {
    if (err) {
      return console.error(err);
    }
    server.start(() => {
      server.log('info', `Server started at ${server.info.uri}`);
    });
  });
