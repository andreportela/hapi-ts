/// <reference path='../typings/index.d.ts' />

'use strict';

import * as Hapi from 'hapi';
import routes from './routes';

export default (port: number): Hapi.Server => {

  const server = new Hapi.Server();

  server.connection({ port: port });
  server.route(routes);

  return server;

};
